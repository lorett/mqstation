# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
import zmq
from datetime import datetime,timedelta

app = Flask(__name__)
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:6009")
end = datetime.utcnow()+timedelta(days=7)
q = {'SATELLITE':['NOAA 18','NOAA 19','BEIJING 1'],'command':'schedule',
     'start':'{}'.format(datetime.utcnow()),'end':'{}'.format(end)}


@app.route('/')
def index():
    res = ''
    socket.send_json(q)
    message = socket.recv_json()
    print(message)
    for n in range(len(message)):
        print(message[n])
        aos = datetime.strptime(message[n]['aos'],"%Y-%m-%d %H:%M:%S.%f")
        los = datetime.strptime(message[n]['los'],"%Y-%m-%d %H:%M:%S.%f")
        res += '<tr><td>{p[satellite]}</td><td>{a:%Y-%m-%d %H:%M:%S}</td><td>{l:%H:%M:%S}</td><td>{p[culminate]}</td></tr>'.format(p=message[n],a=aos,l=los)
        
    return render_template('portal.html',res=res)

if __name__ == "__main__":
    app.run(debug=True, port = 80)

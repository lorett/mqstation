# -*- coding: utf-8 -*-

import zmq
from datetime import datetime,timedelta
import time

conf={'min':60,'up':600,'sleep':1,'end':7}

next_aos = None
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket1 = context.socket(zmq.PUSH)
socket.connect("tcp://localhost:6009")
socket1.connect("tcp://192.168.1.118:6002")
tup = datetime.utcnow()
end = datetime.utcnow()+timedelta(days=7)
q = {'SATELLITE':['NOAA 18','NOAA 19','BEIJING 1','SINAH 1'],'command':'schedule'}
socket.send_json(q)
message = socket.recv_json()
print(message)
next_aos = datetime.strptime(message[0]['aos'],"%Y-%m-%d %H:%M:%S.%f")
los = datetime.strptime(message[0]['los'],"%Y-%m-%d %H:%M:%S.%f")

def update():
    end = datetime.utcnow()+timedelta(days=conf['end'])
    q = {'SATELLITE':['NOAA 18','NOAA 19','BEIJING 1','SINAH 1'],'command':'schedule',
         'start':'{}'.format(datetime.utcnow()),'end':'{}'.format(end)}
    socket.send_json(q)
    message = socket.recv_json()
    return message
    
while True: 
    tnow = datetime.utcnow()
    if next_aos is not None and (tnow + timedelta(seconds=conf['min'])>next_aos):
        if next_aos > tnow:
            socket1.send_json(message[0])
            print(message[0])
            next_aos = None
        else:
            next_aos = None
    else:
        time.sleep(conf['sleep'])
    if los < tnow:
        next_aos = datetime.strptime(message[0]['aos'],"%Y-%m-%d %H:%M:%S.%f")
        los = datetime.strptime(message[0]['los'],"%Y-%m-%d %H:%M:%S.%f")
        message = update()
    if tup + timedelta(seconds=conf['up'])<tnow:
        message = update()
        tup = datetime.utcnow() 
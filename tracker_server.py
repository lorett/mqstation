import time
import zmq
import orbit_predictor.sources
import orbit_predictor.locations
import orbit_predictor.coordinate_systems
from datetime import datetime,timedelta
###################################################################
TLE_FILE='C:\\Users\\User\\.spyder-py3\\MQStation2020\\TLE.txt'
STATION_LAT=55.9308 
STATION_LON=37.5010
MIN_CULMINATION=40
AOS_ELEVATION=70
###################################################################
context=zmq.Context()
socketout=context.socket(zmq.PUB)
socketout.bind('tcp://*:6001')
socketin=context.socket(zmq.PULL)
socketin.bind('tcp://*:6002')
station=orbit_predictor.locations.Location('the_station',STATION_LAT,STATION_LON,0.0)
while True:
    message=socketin.recv_json()
    print(message)
    SATELLITE=message['satellite']
    next_aos = datetime.strptime(message['aos'],"%Y-%m-%d %H:%M:%S.%f")
    los = datetime.strptime(message['los'],"%Y-%m-%d %H:%M:%S.%f")
    tle=orbit_predictor.sources.NoradTLESource.from_file(TLE_FILE)
    tle_lines=tle._get_tle(SATELLITE,datetime.utcnow())
    predictor=tle.get_predictor(SATELLITE)
    #next_pass=predictor.get_next_pass(station,datetime.utcnow(),aos_at_dg=AOS_ELEVATION, max_elevation_gt=MIN_CULMINATION)
    #pos=predictor.get_position(next_pass.aos)
    timen=next_aos
    times=los
    b=''
    print('SATELLITE:', SATELLITE)
    while timen<times:
        pos=predictor.get_position(datetime.utcnow())
        (az,el)=station.get_azimuth_elev_deg(pos)
        b='{} Az={:.2f}  El={:.2f}'.format(datetime.utcnow(),az,el)
        dict={'az':az,'el':el}
        timen+=timedelta(seconds=1)
        print(b)
        time.sleep(1)
        socketout.send_json(dict)
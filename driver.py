import time
import serial
import zmq
import math
import sys
from datetime import datetime

##################################################
PORT='COM6'
AZMN=200
RADSMN=4400/0.504
F=0.745
RADIUS_LIMIT_BOLSHEE=0.45 #0.504
RADIUS_LIMIT_MENSHEE=-0.45#-0.504
##################################################
ser=serial.Serial(PORT,baudrate=9600,timeout=0)
#ser = serial.Serial('COM4',baudrate=9600, timeout=0)
ser.flushInput()
ser.flushOutput()
    
def main():
    print('Connecting to server...')
    context=zmq.Context()
    socketout=context.socket(zmq.SUB)
    socketout.connect('tcp://localhost:6001')
    socketout.setsockopt_string(zmq.SUBSCRIBE,'')
    while True:
        message=socketout.recv_json()
        print(message)
        az=message['az']
        az+=180
        if az>=360:
            az-=360
        if az>180:
            az-=360
        el=message['el']
        el=math.radians(el)
        rads=F*(math.tan(math.radians(90)-el))
        if rads<RADIUS_LIMIT_MENSHEE:
           rads=RADIUS_LIMIT_MENSHEE
        elif rads>RADIUS_LIMIT_BOLSHEE:
           rads=RADIUS_LIMIT_BOLSHEE
        rads*=RADSMN
        az*=AZMN
        timenow=datetime.utcnow()
        v='A {:.2f} {:.2f}\n'.format(az,rads)
        reply=b''
        ser.write(b'S\n')
        time.sleep(0.01)
        ser.write(v.encode('utf-8'))
        print('>')
        while ser.in_waiting > 0:
             c = ser.read(1)
             reply+=c
             #print(c)
             if c==b'\n':
                 if reply!=b'\r\n':
                     print("<<" + reply.decode('utf-8'),end='')
                 #print(reply)
                 reply=b''
        print('{} AZ={:.2f} R={:.2f}'.format(timenow,az,rads))
    
def send_command(command):
    time.sleep(2)
    #c = b"A 22 33\n"
    print(">>{}".format(command))
    #ser.write(b'S\n')
    time.sleep(0.01)
    command += '\n'
    ser.write(command.encode('utf-8'))
    
    reply=b"<"

    time0 = time.time()
    while time.time() - time0 < 3 :
        c = ser.read(1)
        if c != b'':
           #print("{}".format(c.decode("utf-8")),end='' )
           reply += c
           if c == b"\n": 
               print(reply.decode("utf-8"),end='')
               reply = b"<"
               #break
        #time.sleep(0.1) 
       
def ifargvsys():
    com=sys.argv[1]
    azim=float(sys.argv[2])
    radis=float(sys.argv[3])           
    azim+=180
    if azim>=360:
        azim-=360
    if azim>180:
        azim-=360
    radis=math.radians(radis)
    radis=F*(math.tan(math.radians(90)-radis))
    if radis<RADIUS_LIMIT_MENSHEE:
        radis=RADIUS_LIMIT_MENSHEE
    elif radis>RADIUS_LIMIT_BOLSHEE:
        radis=RADIUS_LIMIT_BOLSHEE
    radis*=RADSMN
    azim*=AZMN
    comm='{} {:.2f} {:.2f}'.format(com,azim,radis)
    send_command(comm)
    
if __name__=='__main__':
    if len (sys.argv)>2:
        ifargvsys()
        time.sleep(15)
        #print("{} {} {}".format(sys.argv[1],sys.argv[2],sys.argv[3]))
    elif len(sys.argv)==2:
        send_command(sys.argv[1])
    else:
        main()
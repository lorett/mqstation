import time
import zmq
import orbit_predictor.sources
import orbit_predictor.locations
import orbit_predictor.coordinate_systems
from datetime import datetime,timedelta
context=zmq.Context()
socketout=context.socket(zmq.PUB)
socketout.bind('tcp://*:6001')
TLE_FILE='C:\\Users\\User\\.spyder-py3\\MQStation2020\\TLE.txt'
SATELLITE=input('SATELLITE: ')
STATION_LAT=55.11 
STATION_LON=37.11
MIN_CULMINATION=70  
AOS_ELEVATION=60
station=orbit_predictor.locations.Location('the_station',STATION_LAT,STATION_LON,0.0)
while True:
    tle=orbit_predictor.sources.NoradTLESource.from_file(TLE_FILE)
    tle_lines=tle._get_tle(SATELLITE,datetime.utcnow())
    predictor=tle.get_predictor(SATELLITE)
    next_pass=predictor.get_next_pass(station,datetime.utcnow(),aos_at_dg=AOS_ELEVATION, max_elevation_gt=MIN_CULMINATION)
    pos=predictor.get_position(next_pass.aos)
    timen=next_pass.aos
    times=next_pass.los
    b=''
    #print('SATELLITE:', SATELLITE)
    while timen<times:
        pos=predictor.get_position(timen)
        (az,el)=station.get_azimuth_elev_deg(pos)
        b='{} Az={:.2f}  El={:.2f}'.format(timen,az,el)
        a_dict={}
        aos_d='{}'.format(timen)
        az=round(az,2)
        el=round(el,2)
        a_dict={'aos':aos_d,'az':az,'el':el}
        timen+=timedelta(seconds=1)
        print(a_dict)
        time.sleep(1)
        socketout.send_json(a_dict)
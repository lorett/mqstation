import time
import zmq
import orbit_predictor.sources
import orbit_predictor.locations
import orbit_predictor.coordinate_systems
from datetime import datetime,timedelta
###################################################################
TLE_FILE='C:\\Users\\User\\.spyder-py3\\MQStation2020\\TLE.txt'
STATION_LAT=55.9308 
STATION_LON=37.5010
MIN_CULMINATION=72
AOS_ELEVATION=60
###################################################################
context=zmq.Context()
socketout=context.socket(zmq.PUB)
socketout.bind('tcp://*:6001')
station=orbit_predictor.locations.Location('the_station',STATION_LAT,STATION_LON,0.0)
while True:
    SATELLITE=input('SATELLITE: ')
    #next_aos = datetime.strptime(message['aos'],"%Y-%m-%d %H:%M:%S.%f")
    #los = datetime.strptime(message['los'],"%Y-%m-%d %H:%M:%S.%f")
    tle=orbit_predictor.sources.NoradTLESource.from_file(TLE_FILE)
    tle_lines=tle._get_tle(SATELLITE,datetime.utcnow())
    predictor=tle.get_predictor(SATELLITE)
    next_pass=predictor.get_next_pass(station,datetime.utcnow(),aos_at_dg=AOS_ELEVATION, max_elevation_gt=MIN_CULMINATION)
    pos=predictor.get_position(next_pass.aos)
    timen=next_pass.aos
    delay=timen-timedelta(seconds=30)
    times=next_pass.los
    b=''
    i=0
    while delay<times:
        i+=1
        if i==2:
            print('sleep...')
            time.sleep(30)
        pos=predictor.get_position(timen)
        (az,el)=station.get_azimuth_elev_deg(pos)
        b='{} az={:.2f}  el={:.2f}'.format(timen,az,el)
        dict={'az':az,'el':el}
        timen+=timedelta(seconds=1)
        print(b)
        time.sleep(1)
        socketout.send_json(dict)
import zmq
import time
import math
import sys
import serial
from datetime import datetime
PORT='COM4'
s=serial.Serial(PORT,timeout=4)
context=zmq.Context()
print('Connecting to server...')
socketout=context.socket(zmq.SUB)
socketout.connect('tcp://localhost:6001')
socketout.setsockopt_string(zmq.SUBSCRIBE,'')
F=1.00
while True:
    message=socketout.recv_json()
    az=message['az']
    az+=180
    if az>=360:
        az-=360
    el=message['el']
    el=math.radians(el)
    rads=F*(math.tan(math.radians(90)-el))
    timenow=datetime.utcnow()
    v='utf-8''D {:.2f} {:.2f}\n'.format(az,rads)
    reply=b''
    s.write(v.encode('utf-8'))
    print('>')
    while s.in_waiting > 0:
         c = s.read(1)
         reply+=c
         #print(c)
         if c==b'\n':
             if reply!=b'\r\n':
                 print("<<" + reply.decode('utf-8'),end='')
             #print(reply)
             reply=b''
    print('{} AZ={:.2f} R={:.2f}'.format(timenow,az,rads))
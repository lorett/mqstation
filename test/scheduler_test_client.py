import zmq
from datetime import datetime,timedelta
import time
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:6009")
end = datetime.utcnow()+timedelta(days=7)
q = {'SATELLITE':['NOAA 18','NOAA 19','BEIJING 1'],'command':'schedule',
     'start':'{}'.format(datetime.utcnow()),'end':'{}'.format(end)}
socket.send_json(q)
print(q)
message = socket.recv_json()
print (message)
time.sleep(20)
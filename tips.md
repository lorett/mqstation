#  Полезные советы и ссылки

## FLASK 

*  [Документация Flask](https://flask-russian-docs.readthedocs.io/ru/latest/)
* [Викиучебник](https://ru.wikibooks.org/wiki/Flask)

## Git

* [Git book](https://git-scm.com/book/ru/v2)
* [Скачать git для Windows](https://git-scm.com/download/win )

## ZeroMQ

* [0MQ Guide](http://zguide.zeromq.org/)
* [ZeroMQ. Приступая к работе](https://habr.com/ru/post/198578/)
* [PyZMQ](https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmq.html) 


## Орбитальная механика

* [библиотека Orbit-predictor] (https://pypi.org/project/orbit-predictor/)
* [про TLE] (https://en.wikipedia.org/wiki/Two-line_element_set)
* [источник TLE ] (https://www.celestrak.com/NORAD/elements/)
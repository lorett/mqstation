# -*- coding: utf-8 -*-

import zmq
import time
import orbit_predictor.sources
import orbit_predictor.locations
import orbit_predictor.coordinate_systems
from datetime import datetime,timedelta

conf={'port':6009,'STATION_LAT':55.9308,'STATION_LON':37.5010,'MIN_CULMINATION':10,'AOS_ELEVATION':20,
      'satellits':['NOAA 18','NOAA 19','BEIJING 1','STELLA','SUOMI NPP',
                   'TERRA','MTI','ODIN','METEOR-M 2','LANDSAT 7','SWAS']}

def sсhedule(message):
    TLE_FILE = 'active.tle'
    schedule_passes = []
    
    for i in range(len(conf['satellits'])):
        SATELLITE = conf['satellits'][i]
        tle = orbit_predictor.sources.NoradTLESource.from_file(TLE_FILE)
        
        if ('start' in message) and ('end' in message):
            tnow = datetime.strptime(message['start'],"%Y-%m-%d %H:%M:%S.%f")
            tend = datetime.strptime(message['end'],"%Y-%m-%d %H:%M:%S.%f")
        
        else:
            tnow = datetime.utcnow()
            tend = datetime.utcnow() + timedelta(days=5)
        
        while tnow < tend:
            predictor = tle.get_predictor(SATELLITE)
            next_pass = predictor.get_next_pass(station,tnow,aos_at_dg=conf['AOS_ELEVATION'], 
                                                max_elevation_gt=conf['MIN_CULMINATION'])
            a_pass = {'satellite':SATELLITE, 'aos':'{}'.format(next_pass.aos), 'los':'{}'.format(next_pass.los), 
                      'culminate':float('{:.2f}'.format(next_pass.max_elevation_deg))}
            schedule_passes.append(a_pass)
            print(a_pass)
        
            tnow = next_pass.los
    return schedule_passes

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:{}".format(conf['port']))
station = orbit_predictor.locations.Location('the_station',conf['STATION_LAT'],conf['STATION_LON'],0.0)

while True:
    message = socket.recv_json()
    print ("Recevet request:", message)
    time.sleep(1)
    if message['command'] == 'schedule':
        res = sсhedule(message)
        res.sort(key=lambda a: a['aos'])
        
    socket.send_json(res)